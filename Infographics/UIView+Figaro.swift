//
//  UIView+Figaro.swift
//  Le Figaro
//
//  Created by Konrad Kolakowski on 24.02.2016.
//  Copyright © 2016 Le Figaro. All rights reserved.
//

import UIKit

enum ViewPresentationMode {
    case peek, normal
}

extension UIView {
    func forceHeight(_ height:CGFloat) {
        var heightConst:NSLayoutConstraint?
        
        for constraint in self.constraints {
            if constraint.firstItem as? UIView == self && constraint.firstAttribute == NSLayoutAttribute.height {
                heightConst = constraint
            }
        }
        
        if heightConst != nil && heightConst?.relation != NSLayoutRelation.equal {
            self.removeConstraint( heightConst! )
            heightConst = nil
        }
        
        if heightConst == nil {
            heightConst = NSLayoutConstraint(
                item: self,
                attribute: NSLayoutAttribute.height,
                relatedBy: NSLayoutRelation.equal,
                toItem: nil,
                attribute: NSLayoutAttribute.notAnAttribute,
                multiplier: 1.0,
                constant: height
            )
            self.addConstraint( heightConst! )
        } else {
            heightConst?.constant = height
        }
    }
    
    func bindFrameToViewBounds(_ view: UIView) {
        self.translatesAutoresizingMaskIntoConstraints = false
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[subview]-0-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["subview": self]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[subview]-0-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["subview": self]))
    }
    
    func forceLayout() {
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
    
    class func loadFromNibNamed(_ nibNamed: String, bundle : Bundle? = nil) -> UIView? {
        return UINib(
            nibName: nibNamed,
            bundle: bundle
            ).instantiate(withOwner: nil, options: nil)[0] as? UIView
    }
    
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}
