//
//  ViewController.swift
//  Infographics
//
//  Created by Konrad Kołakowski on 28.10.2016.
//  Copyright © 2016 Le Figaro. All rights reserved.
//

import UIKit

extension String {
    var urlWithHttp: String {
        let http = "http://"
        let https = "https://"
        
        let result: String
        if !self.hasPrefix(http) && !self.hasPrefix(https) {
            result = http + self
        } else {
            result = self
        }
        
        return result
    }
}

class ViewController: UIViewController {
    // MARK: Properties & Outlets
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var webView: FigaroWebView!
    
    // MARK: Initialization
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.errorLabel.isHidden = true
        
        self.searchBar.delegate = self
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.dismissSearchKeyboard))
        self.view.addGestureRecognizer(tapRecognizer)
    }
    
    // MARK: Utilities
    @objc fileprivate func dismissSearchKeyboard() {
        self.searchBar.resignFirstResponder()
    }
    
    fileprivate func setErrorMessage(string: String) {
        let message = "⚠️ " + string
     
        errorLabel.isHidden = false
        errorLabel.text = message
    }
}

extension ViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.dismissSearchKeyboard()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let searchText = searchBar.text?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed), let url = URL(string: searchText) {
            self.webView.targetUrl = url.absoluteString.urlWithHttp
            self.webView.startRequest()
            
            self.errorLabel.isHidden = true
            self.dismissSearchKeyboard()
        } else {
            self.setErrorMessage(string: "Entered text is not a valid URL!")
        }
    }
}
