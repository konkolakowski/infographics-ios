//
//  FigaroWebView.swift
//  Le Figaro
//
//  Created by Konrad Kolakowski on 30.03.2016.
//  Copyright © 2016 Le Figaro. All rights reserved.
//

import UIKit
import WebKit

// MARK: Figaro Web View Delegate
@objc
protocol FigaroWebViewDelegate: NSObjectProtocol {
    func figaroWebViewDidStartLoading(_ figaroWebView: FigaroWebView)
    func figaroWebViewDidFinishLoading(_ figaroWebView: FigaroWebView, withError: NSError?)
    
    @objc optional func figaroWebViewProgressDidChange(_ progress: Float)
}

// MARK: - Figaro Web View
class FigaroWebView: UIView {
    // MARK: Subviews
    fileprivate var loadingView: UIView!
    fileprivate var activityIndicator: UIActivityIndicatorView!
    fileprivate var errorLabel: UILabel!
    
    var webView: WKWebView!
    
    // MARK: Own properties
    var showLoadingOverlay = true
    var targetUrl: String?
    var htmlString: String?
    weak var delegate: FigaroWebViewDelegate?
    
    // MARK: Constants
    fileprivate let WebKitErrorDomain = "WebKitErrorDomain"
    
    static let JavaScriptMetadataInject = "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);"
    
    // MARK: WKWebView thin wrapper
    var allowsBackForwardNavigationGestures: Bool {
        set {
            assert(self.webView != nil)
            self.webView.allowsBackForwardNavigationGestures = newValue
        }

        get {
            assert(self.webView != nil)
            return self.webView.allowsBackForwardNavigationGestures
        }
    }
    
    var canGoBack: Bool {
        assert(self.webView != nil)
        return self.webView.canGoBack
    }
    
    var canGoForward: Bool {
        assert(self.webView != nil)
        return self.webView.canGoForward
    }
    
    func goBack() {
        assert(self.webView != nil)
        self.webView.goBack()
    }
    
    func goForward() {
        self.webView.goForward()
    }
    
    // MARK: Initialization
    fileprivate func buildViewsHierarchy(_ contentFrame: CGRect) {
        // add & setup a web view
        let wkUScript = WKUserScript(source: FigaroWebView.JavaScriptMetadataInject, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        let wkUController = WKUserContentController()
        wkUController.addUserScript(wkUScript)
        
        let wkWebConfig = WKWebViewConfiguration()
        wkWebConfig.userContentController = wkUController
        if #available(iOS 9.0, *) {
            wkWebConfig.websiteDataStore = WKWebsiteDataStore.nonPersistent()
        } else {
            // Fallback on earlier versions
        }

        self.webView = WKWebView(frame: contentFrame, configuration: wkWebConfig)
        self.webView.navigationDelegate = self
        self.addSubview(self.webView)
        
        self.webView.addObserver(self, forKeyPath: "estimatedProgress", options: [NSKeyValueObservingOptions.new], context: nil)
        
        // add & setup loading view
        self.loadingView = UIView(frame: contentFrame)
        self.loadingView.backgroundColor = UIColor.white
        self.loadingView.isHidden = !self.showLoadingOverlay
        self.addSubview(self.loadingView)
        
        self.activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        self.activityIndicator.center = CGPoint(x: self.loadingView.bounds.width / 2.0, y: self.loadingView.bounds.height / 2.0)
        self.loadingView.addSubview(self.activityIndicator)
        
        // add error label
        self.errorLabel = UILabel(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: self.loadingView.bounds.width - 16.0, height: 30.0)))
        self.errorLabel.font = UIFont.systemFont(ofSize: 17.0)
        self.errorLabel.textColor = UIColor.lightGray
        self.errorLabel.textAlignment = .center
        self.errorLabel.adjustsFontSizeToFitWidth = false
        self.errorLabel.lineBreakMode = .byWordWrapping
        self.errorLabel.numberOfLines = 0
        self.errorLabel.center = CGPoint(x: self.loadingView.bounds.width / 2.0, y: self.loadingView.bounds.height / 2.0)
        self.errorLabel.isHidden = true
        self.loadingView.addSubview(self.errorLabel)
    }
    
    convenience init(frame: CGRect, targetUrl: String) {
        self.init(frame: frame)
        self.targetUrl = targetUrl
    }
    
    convenience init(frame: CGRect, htmlString: String) {
        self.init(frame: frame)
        self.htmlString = htmlString
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let contentFrame = CGRect(origin: CGPoint.zero, size: frame.size)
        self.buildViewsHierarchy(contentFrame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        let contentFrame = CGRect(origin: CGPoint.zero, size: frame.size)
        self.buildViewsHierarchy(contentFrame)
    }
    
    deinit {
        if let webview = self.webView {
            webview.stopLoading()
            webview.removeObserver(self, forKeyPath: "estimatedProgress")
        }
    }
    
    // MARK: Layout
    override func layoutSubviews() {
        assert(self.webView != nil)
        assert(self.loadingView != nil)
        assert(self.activityIndicator != nil)
        assert(self.errorLabel != nil)
        
        let contentFrame = CGRect(origin: CGPoint.zero, size: frame.size)
        
        // web view & loading view
        self.webView.frame = contentFrame
        self.loadingView.frame = contentFrame
        
        // activity indicator and error label
        self.activityIndicator.center = CGPoint(x: self.loadingView.bounds.width / 2.0, y: self.loadingView.bounds.height / 2.0)
        
        let errorLabelTargetSize = self.errorLabel.sizeThatFits(CGSize(width: self.loadingView.bounds.width - 16.0, height: CGFloat.greatestFiniteMagnitude))
        self.errorLabel.frame = CGRect(origin: CGPoint.zero, size: errorLabelTargetSize)
        self.errorLabel.center = CGPoint(x: self.loadingView.bounds.width / 2.0, y: self.loadingView.bounds.height / 2.0)
    }
    
    // MARK: Observing Web View
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if let key = keyPath, let changes = change {
            if key == "estimatedProgress" {
                if let progress = changes[NSKeyValueChangeKey(rawValue: "new")] as? NSNumber {
                    self.delegate?.figaroWebViewProgressDidChange?(progress.floatValue)
                }
            }
        }
    }
    
    // MARK: Utilities
    fileprivate func prepareUrl(_ urlString: String!) -> URL? {
        var result: URL?
        
        if urlString != nil {
            // check if we point to something in figaro domain, then
            // "decorate" URL with some parameters
            if urlString.lowercased().contains("figaro.fr") {
                var newUrlString = urlString!
                if newUrlString.contains("?") {
                    newUrlString += "&"
                } else {
                    newUrlString += "?"
                }
                
                newUrlString += "noheader&xtor=AL-209"
                result = URL(string: newUrlString)
            } else {
                result = URL(string: urlString)
            }
        }
        
        return result
    }
    
    fileprivate func startLoading() {
        self.activityIndicator.startAnimating()
        self.activityIndicator.isHidden = false
        
        self.errorLabel.isHidden = true
        
        self.loadingView.isHidden = !self.showLoadingOverlay
    }
    
    fileprivate func stopLoading(_ withError: NSError? = nil) {
        self.activityIndicator.stopAnimating()
        
        if let error = withError {
            self.loadingView.isHidden = false
            
            self.activityIndicator.stopAnimating()
            self.activityIndicator.isHidden = true
            
            // prepare error message
            var errorMessage = "Impossible de charger le site web: \(error.localizedDescription)"
            if error.domain == WebKitErrorDomain && error.code == 102 {
                errorMessage = "La page est ouverte dans une application externe"
            }
            
            self.errorLabel.isHidden = false
            self.errorLabel.text = errorMessage
            
            print( "Error: " + error.description)
        } else {
            self.errorLabel.isHidden = true
            self.loadingView.isHidden = true
        }
        
        self.forceLayout()
    }
    
    // MARK: Operations
    func startRequest() {
        if self.targetUrl != nil && self.htmlString != nil {
            print("You can't pass both URL and HTML string to FigaroWebViewController! URL will be used...")
        }
        
        if let url = self.prepareUrl(self.targetUrl) {
            let request = URLRequest(url: url)
            self.webView.load(request)
        } else if let html = self.htmlString {
            self.webView.loadHTMLString(html, baseURL: nil)
        } else {
            self.loadingView.isHidden = true
        }
    }
}

// MARK: - WKNavigationDelegate delegate
extension FigaroWebView: WKNavigationDelegate {
    // navigation policy
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        // handle external URL's
        if let url = navigationAction.request.url, let host = url.host {
            // iTunes/AppStore link
            if host.contains("itunes.apple.com") {
                UIApplication.shared.openURL(url)
                decisionHandler(.cancel)
                return
            }
            
            // Protocol URL-scheme without http(s)
            if url.scheme != "http" && url.scheme != "https" {
                UIApplication.shared.openURL(url)
                decisionHandler(.cancel)
                return
            }
        }
        
        // handle target="_blank", because by default WKWebView ignores links that open
        // in new window
        if navigationAction.targetFrame == nil {
            webView.stopLoading()
            webView.load(navigationAction.request)
        }
        
        decisionHandler(.allow)
    }
    
    // start loading
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        self.startLoading()
        
        self.delegate?.figaroWebViewDidStartLoading(self)
    }
    
    // finish loading, error or not
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.stopLoading()
        
        self.delegate?.figaroWebViewDidFinishLoading(self, withError: nil)
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        let nsError = error as NSError
        if nsError.code != URLError.cancelled.rawValue {
            self.stopLoading(error as NSError?)
            self.delegate?.figaroWebViewDidFinishLoading(self, withError: error as NSError?)
        } else {
            self.stopLoading()
            self.delegate?.figaroWebViewDidFinishLoading(self, withError: nil)
        }
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        let nsError = error as NSError
        if nsError.code != URLError.cancelled.rawValue {
            self.stopLoading(error as NSError?)
            self.delegate?.figaroWebViewDidFinishLoading(self, withError: error as NSError?)
        } else {
            self.stopLoading()
            self.delegate?.figaroWebViewDidFinishLoading(self, withError: nil)
        }
    }
}
